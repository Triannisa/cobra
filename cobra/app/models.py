from django.db import models
from django.utils import timezone

# Create your models here.
class Users(models.Model):
    name = models.CharField(max_length=100)
    key_secret = models.CharField(max_length=16)
    status = models.BooleanField()
    user_type = models.CharField(max_length=20)
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.name

class Kos(models.Model):
   user_id = models.ForeignKey(Users,on_delete = models.CASCADE, default=0)
   nama = models.CharField(max_length=100, default="")
   alamat = models.CharField(max_length=255, default="")
   kapasitas = models.IntegerField(default=0)
   terisi = models.IntegerField(default=0)
   harga = models.IntegerField(default=0)
   
   def __str__(self):
        return self.nama

class Bill(models.Model):
    id_user = models.ForeignKey('app.Users', on_delete = models.CASCADE, default = 0)
    pendapatan = models.FloatField(default=0)
    pajak = models.FloatField(default=0)