from django.contrib import admin
from .models import *

# Register your models here.

class UsersAdmin(admin.ModelAdmin):
    list_display = ('name', 'key_secret', 'status', 'user_type', 'created_at', 'updated_at') 

class KosAdmin(admin.ModelAdmin):
    list_display = ('nama', 'alamat', 'kapasitas', 'terisi', 'harga')

class BillAdmin(admin.ModelAdmin):
    list_display = ('id_user', 'pendapatan', 'pajak')

admin.site.register(Users, UsersAdmin)
admin.site.register(Kos, KosAdmin)
admin.site.register(Bill, BillAdmin)